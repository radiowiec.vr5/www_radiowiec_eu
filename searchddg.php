<?php
$term = $_GET["query"]; // This line gets the search term submitted by the user
// $term = preg_replace('/[^A-Za-z0-9-s]/', '', $term); // This line uses a regex to remove anything other than letters and numbers from the term so we don't break our search
// $term = str_replace(" ","+",$term); // Replaces spaces with pluses
$term = urlencode($term);
$urlstub = "https://duckduckgo.com/?q=site:www.radiowiec.eu+"; // Replace fabulouspanda.com with your own domain. This adds the formatted search term to the duckduckgo url
header('Location:'.$urlstub.$term); // redirect the user to our custom search 
die(); //make sure to kill the process now that it's all done
?>